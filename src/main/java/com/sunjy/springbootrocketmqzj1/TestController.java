package com.sunjy.springbootrocketmqzj1;

import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.starter.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by sunjy on 2018/9/25.
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    public void test1() throws Exception {
        Message message =new Message();
        message.setTopic("topic-1");
        message.setTags("A");
        message.setBody("jjjjjjjjjjj".getBytes());

        rocketMQTemplate.asyncSend("topic-1",message,new SendCallback(){

            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("SUCESS");
            }

            @Override
            public void onException(Throwable throwable) {
                System.out.println("FAIL");
            }
        });
    }
}
