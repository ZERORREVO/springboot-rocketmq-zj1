package com.sunjy.springbootrocketmqzj1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRocketmqZj1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRocketmqZj1Application.class, args);
	}
}
