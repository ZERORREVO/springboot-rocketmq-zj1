package com.sunjy.springbootrocketmqzj1.mq;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.spring.starter.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.starter.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * Created by sunjy on 2018/9/25.
 */
@Service
@RocketMQMessageListener(topic = "topic-1", consumerGroup = "test-1"/*,selectorExpress="A"*/)
public class MyConsumer1 implements RocketMQListener<Object> {
    public void onMessage(Object message) {
        System.out.println("received message: {}"+ JSON.toJSON(message));
    }
}
